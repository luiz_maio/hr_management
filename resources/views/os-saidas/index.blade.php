@extends('template')

@section('title')
	OS - Tipos de Saída
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Tipos de Saída</h1>
        {{ Html::link('os/saidas/novo', 'Novo Tipo de Saída', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($saidas) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Nome</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($saidas as $saida)
    				<tr>
    					<td>{{ $saida->nome }}</td>
    					<td>
    						<a href="saidas/edit/{{ $saida->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							Editar
    						</a>
                            <a href="saidas/remove/{{ $saida->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse tipo de saída?');">
    							Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhum Tipo de Saída Localizado</h4>
    	</div>
    @endif

    </div>

@stop
