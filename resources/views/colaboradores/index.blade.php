@extends('template')

@section('title')
	Colaboradores
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Colaboradores</h1>
        {{ Html::link('colaboradores/nova', 'Novo Colaborador', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($colaboradores) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Nome</th>
    				<th>Cargo</th>
					<th>Avaliador</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($colaboradores as $colaborador)
    				<tr>
    					<td>{{ $colaborador->nome . ' ' . $colaborador->sobrenome }}</td>
						<td>{{ $colaborador->cargo }}</td>
    					<td>{{ $colaborador->nome_aval }}</td>
    					<td>
    						<a href="colaboradores/edit/{{ $colaborador->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="colaboradores/remove/{{ $colaborador->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse colaborador?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhum Coolaborador Localizado</h4>
    	</div>
    @endif

    </div>

@stop
