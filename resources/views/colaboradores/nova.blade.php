@extends('template')

@section('title')
	Colaboradores
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Novo Colaborador</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblSobrenome', 'Sobrenome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('sobrenome', '', array('class' => 'form-control', 'id' => 'editSobrenome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblEmail', 'Email', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('email', '', array('class' => 'form-control', 'id' => 'editEmail')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblCargo', 'Cargo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('cargo', '', array('class' => 'form-control', 'id' => 'editCargo')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblDepartamento', 'Departamento', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('depto', '', array('class' => 'form-control', 'id' => 'editDepartamento')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblAvaliador', 'Avaliador', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('nome_aval', $avaliadores, null, array('placeholder' => 'Selecione o Avaliador...', 'class' => 'form-control')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				<span class="col-sm-1 control-label"></span>
                {!! Form::checkbox('is_aval', 1, false) !!} &nbsp;
                O colaborador é um avaliador
            </div>
    		<div class="modalButtons">
				{{ Html::link('colaboradores', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
