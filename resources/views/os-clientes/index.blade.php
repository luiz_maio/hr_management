@extends('template')

@section('title')
	OS - Clientes
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Clientes</h1>
        {{ Html::link('os/clientes/novo', 'Novo Cliente', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($clientes) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Nome</th>
    				<th>Sigla</th>
					<th>Status</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($clientes as $cliente)
    				<tr>
    					<td>{{ $cliente->nome }}</td>
						<td>{{ $cliente->sigla }}</td>
    					<td>{{ $cliente->status ? 'Ativo' : 'Inativo' }}</td>
    					<td>
    						<a href="clientes/edit/{{ $cliente->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="clientes/remove/{{ $cliente->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse cliente?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhum Cliente Localizado</h4>
    	</div>
    @endif

    </div>

@stop
