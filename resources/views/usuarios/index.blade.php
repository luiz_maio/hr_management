@extends('template')

@section('title')
	Usuários
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Usuários</h1>
        {{ Html::link('usuarios/nova', 'Novo Usuário', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($users) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
					<th>Nome</th>
                    <th>Perfil</th>
    				<th>Status</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($users as $user)
    				<tr>
						<td>{{ $user->nome . ' ' . $user->sobrenome }}</td>
    					<td>{{ $user->perfis->nome }}</td>
						<td>{{ $user->status ? 'Ativo' : 'Inativo' }}</td>
    					<td>
    						<a href="usuarios/edit/{{ $user->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="usuarios/remove/{{ $user->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse usuário?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhum Usuário Localizado</h4>
    	</div>
    @endif

    </div>

@stop
