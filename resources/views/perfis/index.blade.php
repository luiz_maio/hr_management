@extends('template')

@section('title')
	Perfis
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Perfis</h1>
        {{ Html::link('perfis/nova', 'Novo Perfil', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($profiles) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Nome</th>
    				<th>Status</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($profiles as $profile)
    				<tr>
    					<td>{{ $profile->nome . ' ' . $profile->sobrenome }}</td>
						<td>{{ $profile->status ? 'Ativo' : 'Inativo' }}</td>
    					<td>
    						<a href="perfis/edit/{{ $profile->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="perfis/remove/{{ $profile->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse perfil?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhum Perfil Localizado</h4>
    	</div>
    @endif

    </div>

@stop
