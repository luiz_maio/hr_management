@extends('template')

@section('title')
	OS - Parâmetros
@stop

@section('content')

	<script>
		$(function(){
			$("#TipoCampo").change( function() {
				console.log($(this).val());
				if($(this).val() == 3){
					$('#box-valor').show();
				}
				else{
					$('#box-valor').hide();
				}
		    });
		});
	</script>

    <div class="list-title-header">
    	<h1>Novo Parâmetro</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-2 control-label')) !!}
				<div class="col-sm-10">
			    	{!! Form::text('nome', '', array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblSigla', 'Sigla XML', array('class' => 'col-sm-2 control-label')) !!}
				<div class="col-sm-10">
			    	{!! Form::text('sigla', '', array('class' => 'form-control', 'id' => 'editSigla')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-2 control-label')) !!}
				<div class="col-sm-10">
			    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), null, array('class' => 'form-control', 'id' => 'editStatus')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblTipoCampo', 'Tipo de Campo', array('class' => 'col-sm-2 control-label')) !!}
				<div class="col-sm-10">
			    	{!! Form::select('id_tipo_campo', $tiposCampos, null, array('class' => 'form-control', 'id' => 'TipoCampo')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both; display: none;" id="box-valor">
				{!! Form::label('lblValor', 'Valor Padrão', array('class' => 'col-sm-2 control-label')) !!}
				<div class="col-sm-10">
			    	{!! Form::text('valor', '', array('class' => 'form-control', 'id' => 'editValor')) !!}
		    	</div>
		    </div>
			<div class="form-group" id="listClientes">
				{!! Form::label('lblClientes', 'Clientes', array('class' => 'col-sm-2 control-label')) !!}
				<?php $i = 1; ?>
				@foreach($clientes as $key => $cliente)
					<div class="col-sm-2 control-label" style="text-align: left">
						{!! Form::checkbox('clientes[]', $key, null, array('id' => 'editCliente'.$key)) !!}
						{{ $cliente }}
						@if($i % 4 == 0)
							<br /><br />
							{!! Form::label('lblCores', ' ', array('class' => 'col-sm-2 control-label')) !!}
						@endif
						<?php $i++; ?>
					</div>
				@endforeach
			</div>
    		<div class="modalButtons">
				{{ Html::link('os/parametros', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
