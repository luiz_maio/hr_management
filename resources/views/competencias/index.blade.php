@extends('template')

@section('title')
	Competências
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Competências</h1>
        {{ Html::link('competencias/nova', 'Nova Competência', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($competencias) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Descrição</th>
    				<th>Ordem Exibição</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($competencias as $competencia)
    				<tr>
    					<td>{{ str_limit($competencia->descricao, 70) }}</td>
    					<td>{{ $competencia->ordem }}</td>
    					<td>
    						<a href="competencias/edit/{{ $competencia->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							<span class="glyphicon glyphicon-edit"></span> Editar
    						</a>
                            <a href="competencias/remove/{{ $competencia->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover essa competência?');">
    							<span class="glyphicon glyphicon-trash"></span> Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhuma Competência Localizada</h4>
    	</div>
    @endif

    </div>

@stop
