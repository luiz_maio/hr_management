@extends('template')

@section('title')
	OS - Serviços
@stop

@section('content')

	<script src="{{ asset('/js/os-servicos-clientes.js') }}"></script>

    <div class="list-title-header">
    	<h1>Serviços</h1>
		<div class="list-header">
			<div class="input-group col-xs-6 search" style="float:left;">
				<input type="text" id="txt-search" class="form-control" placeholder="Buscar por Código ou Rótulo ...">

				<span class="input-group-btn">
					<button id="btn-search" class="btn btn-default" type="button">Buscar</button>
				</span>
			</div>
			<div class="col-sm-3" style="float:left;">
				{!! Form::select('cliente', $clientes, null, array('placeholder' => 'Filtro por Cliente...', 'class' => 'form-control', 'id' => 'filtroCliente')) !!}
			</div>
	        {{ Html::link('os/servicos/novo', 'Novo Serviço', array('class' => 'btn btn-info',  'style' => 'float:right')) }}
		</div>
    </div>

    <div class="table-responsive clear">


    @if (count($servicos) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Rótulo</th>
					<th>Job</th>
    				<th>Código</th>
					<th>Data de Entrada</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody id="tabela-corpo">
    			@foreach($servicos as $servico)
    				<tr>
    					<td style="width: 30%;">{{ $servico->rotulo }}</td>
						<td>{{ $servico->job }}</td>
						<td>{{ $servico->codigo }}</td>
    					<td>{{ $servico->dt_entrada }}</td>
    					<td>
    						<a href="servicos/edit/{{ $servico->id }}" id="btn-edit" class="btn btn-primary btn-sm">
    							Editar
    						</a>
                            <a href="servicos/remove/{{ $servico->id }}" id="btn-remove" class="btn btn-danger btn-sm" onclick="return confirm('Deseja remover esse serviço?');">
    							Remover
    						</a>
							<a href="servicos/print/{{ $servico->id }}" id="btn-print" target='_blank' class="btn btn-info btn-sm">
    							Imprimir OS
    						</a>
							<a href="servicos/send/{{ $servico->id }}" id="btn-send" class="btn btn-warning btn-sm" onclick="return confirm('Deseja enviar essa OS?');">
    							Enviar XML
    						</a>
							<a href="servicos/novo/{{ $servico->id }}" id="btn-send" class="btn btn-success btn-sm">
    							Duplicar OS
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhum Serviço Localizado</h4>
    	</div>
    @endif

    </div>

@stop
