@extends('template')

@section('title')
	OS - Serviços
@stop

@section('content')

	<script src="{{ asset('/js/os-servicos.js') }}"></script>

    <div class="list-title-header">
    	<h1>Novo Serviço</h1>
    </div>
	<div class="messages">
		@if (session('erro'))
			<div class="alert-success">
				{{ session('erro') }}
			</div>
		@endif
	</div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group">
				{!! Form::label('lblCliente', 'Cliente', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::select('id_cliente', $clientes, null, array('placeholder' => 'Selecione o Cliente...', 'class' => 'form-control', 'id' => 'editClientes')) !!}
		    	</div>
				{!! Form::label('lblSaida', 'Saída', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::select('tipo_saida', $saidas, null, array('placeholder' => 'Selecione o Tipo de Saída...', 'class' => 'form-control', 'id' => 'editTipoSaida')) !!}
		    	</div>
				{!! Form::label('lblVolume', 'Volume', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::select('volume', $volumes, null, array('placeholder' => 'Selecione o Volume...', 'class' => 'form-control', 'id' => 'editVolume')) !!}
		    	</div>
		    </div>

			<div class="form-group">
				{!! Form::label('lblRotulo', 'Rótulo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
			    	{!! Form::text('rotulo', '', array('class' => 'form-control', 'id' => 'editSigla')) !!}
		    	</div>
				{!! Form::label('lblJob', 'Num. Job', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
					{!! Form::text('job_num', '', array('class' => 'form-control', 'id' => 'editJobNum', 'placeholder' => '00-1234')) !!}
				</div>
				<div class="col-sm-1">
					{!! Form::text('job_cli', '', array('class' => 'form-control', 'id' => 'editJobCli', 'readonly')) !!}
				</div>
				{!! Form::label('lblRev', 'Revisão', array('class' => 'col-sm-1 control-label', )) !!}
				<div class="col-sm-1">
					{!! Form::text('job_rev', '', array('class' => 'form-control', 'id' => 'editRevisao')) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('lblEntrada', 'Data', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
					{!! Form::text('dt_entrada', '', array('class' => 'form-control', 'id' => 'editEntrada', 'placeholder' => 'dd/mm/aaaa')) !!}
				</div>
				<div id="campo-exal" style="display: none;">
					{!! Form::label('lblCodExal', 'Cód. Exal', array('class' => 'col-sm-1 control-label', 'style' => 'width: 8%;')) !!}
					<div class="col-sm-2">
						{!! Form::text('codigo_exal', '', array('class' => 'form-control', 'id' => 'editCodExal')) !!}
					</div>
				</div>
				{!! Form::label('lblCodigo', 'Cód. Cliente', array('class' => 'col-sm-1 control-label', 'style' => 'width: 8%;')) !!}
				<div class="col-sm-2">
					{!! Form::text('codigo', '', array('class' => 'form-control', 'id' => 'editCodigo')) !!}
				</div>
				{!! Form::label('lblJobAnterior', 'Job Anterior', array('class' => 'col-sm-1 control-label', 'style' => 'width: 9%;')) !!}
				<div class="col-sm-2">
					{!! Form::text('job_anterior', '', array('class' => 'form-control', 'id' => 'editJobAnterior', 'placeholder' => '00-1234-XX-R1', 'disabled')) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('lblEntrada', 'Contato 1', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-5">
					{!! Form::text('contato_um', '', array('class' => 'form-control', 'id' => 'editContato1', 'placeholder' => 'contato@email.com')) !!}
				</div>
				{!! Form::label('lblCodigo', 'Contato 2', array('class' => 'col-sm-1 control-label', 'style' => 'width: 8%;')) !!}
				<div class="col-sm-5">
					{!! Form::text('contato_dois', '', array('class' => 'form-control', 'id' => 'editContato2', 'placeholder' => 'contato@email.com')) !!}
				</div>
			</div>

			<div class="form-group" id="listFabricas">
				{!! Form::label('lblFabricas', 'Fábricas', array('class' => 'col-sm-1 control-label')) !!}
				<div id="lista-fabricas">

				</div>
			</div>

			<div class="form-group">
				{!! Form::label('lblCliente', 'Observações', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::textarea('observacoes', '', array('class' => 'form-control', 'id' => 'editObs')) !!}
		    	</div>
		    </div>

			<hr>

			<div class="form-group" id="box-cores">
				{!! Form::label('lblCores', 'Cores', array('class' => 'col-sm-1 control-label')) !!}
				@for ($i = 1; $i < 9; $i++)
					<div class="col-sm-2" style="width: 22%;">
						{!! Form::text('cores[]', '', array('class' => 'form-control', 'id' => 'editCores')) !!}
					</div>
					@if($i % 4 == 0)
						<br /><br />
						{!! Form::label('lblCores', ' ', array('class' => 'col-sm-1 control-label')) !!}
					@endif
				@endfor
				{!! Form::label('lblCores', ' ', array('class' => 'col-sm-1 control-label')) !!}
				{!! Form::button('Carregar Cores', array('class' => 'btn btn-warning', 'style' => 'float: right; margin-right: 58px;', 'id' => 'btn-cores')) !!}
			</div>

			<hr>

			<div class="form-group" id="listParametros">
				<div id="lista-parametros">

				</div>
			</div>

    		<div class="modalButtons">
				{{ Html::link('os/servicos', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
