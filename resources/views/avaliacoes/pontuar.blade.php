@extends('template')

@section('title')
	Avaliações
@stop

@section('content')

	<script src="{{ asset('/js/pontuar.js') }}"></script>

    <div class="list-title-header">
    	<h1>Pontuar Colaborador</h1>
    </div>

	<div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblColaborador', 'Colaborador(a)', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::text('colaborador', $editAvaliacao->colaboradores->nome . ' ' . $editAvaliacao->colaboradores->sobrenome, array('class' => 'form-control', 'id' => 'editColaborador', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblCargo', 'Cargo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('cargo', $editAvaliacao->colaboradores->cargo, array('class' => 'form-control', 'id' => 'editCargo', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblDepartamento', 'Departamento', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('departamento', $editAvaliacao->colaboradores->depto, array('class' => 'form-control', 'id' => 'editDepartamento', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblAvaliador', 'Avaliador(a)', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
					{!! Form::text('avaliador', $editAvaliacao->avaliadores->nome . ' ' . $editAvaliacao->avaliadores->sobrenome, array('class' => 'form-control', 'id' => 'editAvaliador', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblData', 'Data da Avaliação', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('data_avaliacao', $editAvaliacao->data_avaliacao, array('class' => 'form-control', 'id' => 'editData', 'placeholder' => 'DD/MM/AAAA')) !!}
		    	</div>
		    </div>

			<div style="margin: 30px 0; width: 100%; border: solid 1px #333333; padding: 20px 10px; line-height: 25px;">
				<b>INSTRUÇÕES:</b> <br />
				<p><small>
					> Avalie os colaboradores individualmente em cada uma das competências, marcando um "X" em uma das cinco classificações. <br />
					> Para competências não necessárias ao cargo avaliado, marcar a opção "Não Aplicável". <br />
					> Converse com o colaborador sobre a sua avaliação, abordando os pontos fortes e também aqueles que precisam ser desenvolvidos.
				</small></p>
				<b>LEGENDAS:</b> <br />
				<p><small>
					> EXCELENTE: Supera as expectativas.<br />
					> BOM: Dentro das expectativas.<br />
					> REGULAR: Abaixo das expectativas com perspectiva de melhora.<br />
					> RUIM: Abaixo das expectativas e sem perspectiva de melhora.<br />
					> NÃO APLICÁVEL: Item não se aplica ao cargo avaliado.<br />
				</small></p>
			</div>

			<h4 style="margin-bottom: 20px;">1. COMPETÊNCIAS TÉCNICAS E COMPORTAMENTAIS</h4>

			@foreach($competencias as $competencia)
				<table id="list-produtos" class="table table-striped">
					<thead>
						<tr>
							<th>{{ $competencia->descricao }}</th>
							@foreach($notas as $nota)
								<th>{{ $nota->descricao }}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach($competencia->itensCompetencias as $itemCompetencia)
							<tr>
								<td style="width: 760px;">{{ $itemCompetencia->descricao }}</td>
								@foreach($notas as $nota)
									@if($nota->id == $itemCompetencia->nota)
										<td style="text-align: center;">{{ Form::radio("notas[$itemCompetencia->id]", $nota->id, true) }}</td>
									@else
										<td style="text-align: center;">{{ Form::radio("notas[$itemCompetencia->id]", $nota->id) }}</td>
									@endif
								@endforeach
							</tr>
						@endforeach
					</tbody>
				</table>
			@endforeach

			<h4 style="margin-top: 30px;">2. RESULTADO</h4>

			<div class="form-group" style="clear: both;">
				{!! Form::label('lblExcelente', 'Excelente', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
			    	{!! Form::text('result_excelente', '', array('class' => 'form-control', 'id' => 'editExcelente', 'disabled')) !!}
		    	</div>
				{!! Form::label('lblBom', 'Bom', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
			    	{!! Form::text('result_bom', '', array('class' => 'form-control', 'id' => 'editBom', 'disabled')) !!}
		    	</div>
				{!! Form::label('lblRegular', 'Regular', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
			    	{!! Form::text('result_regular', '', array('class' => 'form-control', 'id' => 'editRegular', 'disabled')) !!}
		    	</div>
				{!! Form::label('lblRuim', 'Ruim', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
			    	{!! Form::text('result_ruim', '', array('class' => 'form-control', 'id' => 'editRuim', 'disabled')) !!}
		    	</div>
		    </div>

			<h4 style="margin-top: 30px;">3. AÇÃO RECOMENDADA</h4>

			<small>
				<b>3.1</b>&nbsp;
				Somente Avaliação de Desempenho <br />
				<b>Excelente</b><br />
				<b>Bom</b>
				<br /><br />
				<b>3.2</b>&nbsp;
				Colocar o Colaborador em observação e rever novamente em: <br />
				<b>Regular</b> <br />
				<b>Ruim</b>
			</small>

			<div class="form-group" style="clear: both;">
				{!! Form::label('lblResultado', 'Resultado', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-2">
			    	{!! Form::text('nota_acao', $editAvaliacao->nota_acao, array('class' => 'form-control', 'id' => 'editNotaAcao', 'readonly')) !!}
		    	</div>
				{!! Form::label('lblObs', 'Ação', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-8">
			    	{!! Form::text('obs_acao', $editAvaliacao->obs_acao, array('class' => 'form-control', 'id' => 'editObsAcao')) !!}
		    	</div>
		    </div>

			<h4 style="margin-top: 30px;">4. OBSERVAÇÕES</h4>

			<div class="form-group" style="clear: both;">
				{!! Form::label('lblObs', 'Comentários Gerais', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::textarea('obs_finais', $editAvaliacao->obs_finais, array('class' => 'form-control', 'id' => 'editObsAcao')) !!}
		    	</div>
		    </div>

    		<div class="modalButtons">
				{!! Form::button('Pontuar', array('class' => 'btn btn-info', 'id' => 'btn-pontuar')) !!}
				{{ Html::link('avaliacoes', 'Cancelar', array('class' => 'btn btn-warning')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
