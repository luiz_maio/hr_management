@extends('template')

@section('title')
	Avaliações
@stop

@section('content')

<script>

	$(function () {
		$("#editColaborador").change( function() {
			$.ajax({
				type: 'get',
				url: window.location.origin + '/servicos/colaboradores/' + $(this).val(),
				context: document.body
			}).done(function(data){
				$('#editCargo').val(data.colaborador.cargo);
				$('#editDepartamento').val(data.colaborador.depto);
			});
		});
	});

</script>

    <div class="list-title-header">
    	<h1>Nova Avaliação</h1>
    </div>

    <div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblColaborador', 'Colaborador', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('id_colaborador', $colaboradores, null, array('placeholder' => 'Selecione o Colaborador...', 'class' => 'form-control', 'id' => 'editColaborador')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblCargo', 'Cargo', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('cargo', '', array('class' => 'form-control', 'id' => 'editCargo', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblDepartamento', 'Departamento', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('departamento', '', array('class' => 'form-control', 'id' => 'editDepartamento', 'disabled')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblAvaliador', 'Avaliador', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('id_avaliador', $avaliadores, null, array('placeholder' => 'Selecione o Avaliador...', 'class' => 'form-control')) !!}
		    	</div>
		    </div>
    		<div class="modalButtons">
				{{ Html::link('avaliacoes', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
