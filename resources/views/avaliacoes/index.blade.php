@extends('template')

@section('title')
	Avaliações
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Avaliações</h1>
        {{ Html::link('avaliacoes/nova', 'Nova Avaliação', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($avaliacoes) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Colaborador</th>
    				<th>Avaliador</th>
					<th>Data de Avaliação</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($avaliacoes as $avaliacao)
    				<tr>
						<td>{{ $avaliacao->colaboradores->nome . ' ' . $avaliacao->colaboradores->sobrenome }}</td>
    					<td>{{ $avaliacao->avaliadores->nome . ' ' . $avaliacao->avaliadores->sobrenome }}</td>
						<td>{{ $avaliacao->data }}</td>
    					<td>
    						<a href="avaliacoes/edit/{{ $avaliacao->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							Editar
    						</a>
							<a href="avaliacoes/pontuar/{{ $avaliacao->id }}" id="btn-edit" class="btn btn-success btn-sm btn-success">
    							Pontuar
    						</a>
							<a href="avaliacoes/remove/{{ $avaliacao->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover essa avaliação?');">
								Remover
							</a>
							@if (!empty($avaliacao->nota_acao))
								<a href="avaliacoes/print/{{ $avaliacao->id }}" id="btn-print" class="btn btn-info btn-sm btn-info" target="_blank">
									Imprimir
								</a>
							@endif
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhuma Avaliação Localizada</h4>
    	</div>
    @endif

    </div>

@stop
