@extends('template')

@section('title')
	OS - Volumes
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Volumes</h1>
        {{ Html::link('os/volumes/novo', 'Novo Volume', array('class' => 'btn btn-info')) }}
    </div>

    <div class="table-responsive clear">

    @if (count($volumes) >= 1)
    	<table id="list-package" class="table table-striped">
    		<thead>
    			<tr>
                    <th>Nome</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<tbody>
    			@foreach($volumes as $volume)
    				<tr>
    					<td>{{ $volume->nome }}</td>
    					<td>
    						<a href="volumes/edit/{{ $volume->id }}" id="btn-edit" class="btn btn-primary btn-sm btn-edit">
    							Editar
    						</a>
                            <a href="volumes/remove/{{ $volume->id }}" id="btn-remove" class="btn btn-danger btn-sm btn-danger" onclick="return confirm('Deseja remover esse volume?');">
    							Remover
    						</a>
    					</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	<div>
    		<h4>Nenhum Volume Localizado</h4>
    	</div>
    @endif

    </div>

@stop
