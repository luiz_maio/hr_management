@extends('template')

@section('title')
	OS - Clientes
@stop

@section('content')

    <div class="list-title-header">
    	<h1>Editar Cliente</h1>
    </div>

	<div class="">
		{!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-package')) !!}
			{{ Form::hidden('id', '', array('id' => 'editId')) }}
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblNome', 'Nome', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('nome', $editFabrica->nome, array('class' => 'form-control', 'id' => 'editNome')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblSigla', 'Sigla', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::text('sigla', $editFabrica->sigla, array('class' => 'form-control', 'id' => 'editSigla')) !!}
		    	</div>
		    </div>
			<div class="form-group" style="clear: both;">
				{!! Form::label('lblStatus', 'Status', array('class' => 'col-sm-1 control-label')) !!}
				<div class="col-sm-11">
			    	{!! Form::select('status', array('1' => 'Ativo', '0' => 'Inativo'), $editFabrica->sigla, array('class' => 'form-control', 'id' => 'editStatus')) !!}
		    	</div>
		    </div>
			<div class="form-group" id="listClientes">
				{!! Form::label('lblClientes', 'Clientes', array('class' => 'col-sm-1 control-label')) !!}
				<?php $i = 1; ?>
				@foreach($clientes as $key => $cliente)
					<div class="col-sm-2 control-label" style="text-align: left">
						@if(in_array($key, $clientesIds))
							{!! Form::checkbox('clientes[]', $key, true, array('id' => 'editCliente'.$key)) !!}
						@else
							{!! Form::checkbox('clientes[]', $key, null, array('id' => 'editCliente'.$key)) !!}
						@endif
						{{ $cliente }}
						@if($i % 5 == 0)
							<br /><br />
							{!! Form::label('lblCores', ' ', array('class' => 'col-sm-1 control-label')) !!}
						@endif
						<?php $i++; ?>
					</div>
				@endforeach
			</div>
    		<div class="modalButtons">
				{{ Html::link('os/fabricas', 'Cancelar', array('class' => 'btn btn-info')) }}
			    {!! Form::submit('Salvar', array('class' => 'btn btn-success')) !!}
			</div>

		{!! Form::close() !!}
	</div>

@stop
