<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfis extends Model
{
    protected $fillable = array(
        'nome',
        'status'
    );

    public function usuarios()
    {
    	return $this->hasMany('App\Usuarios', 'id_perfil');
    }

}
