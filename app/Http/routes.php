<?php

Route::group(['middleware' => ['web']], function () {

    Route::get('/', array('as' => 'login', 'uses' => 'LoginController@index'));
    Route::post('/', array('as' => 'login', 'uses' => 'LoginController@postLogin'));

    // Rotas de Usuários Autenticados
    Route::group(['middleware' => 'auth'], function () {

        Route::get('/logout', function() {
    		Auth::logout();
    		return Redirect::to('/');
    	});

        Route::group(['prefix' => 'avaliacoes'], function () {
            Route::get('/', array('as' => 'avaliacoes.index', 'uses' => 'AvaliacaoController@index'));
            Route::get('/pontuar/{id}', array('as' => 'avaliacoes.pontuar', 'uses' => 'AvaliacaoController@getPontuar'));
            Route::post('/pontuar/{id}', array('as' => 'avaliacoes.pontuar', 'uses' => 'AvaliacaoController@postPontuar'));
            Route::get('/print/{id}', array('as' => 'avaliacoes.imprimir', 'uses' => 'PrintController@getPrint'));
        });

        Route::group(['prefix' => 'os'], function () {
            Route::group(['prefix' => 'clientes'], function () {
                Route::get('/', array('as' => 'os.clientes.index', 'uses' => 'OsClienteController@index'));
                Route::get('/novo', array('as' => 'os.clientes.get.novo', 'uses' => 'OsClienteController@getNova'));
                Route::post('/novo', array('as' => 'os.clientes.post.novo', 'uses' => 'OsClienteController@postNova'));
                Route::get('/edit/{id}', array('as' => 'os.clientes.get.edit', 'uses' => 'OsClienteController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'os.clientes.post.edit', 'uses' => 'OsClienteController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'os.clientes.remove', 'uses' => 'OsClienteController@getRemove'));
            });

            Route::group(['prefix' => 'fabricas'], function () {
                Route::get('/', array('as' => 'os.fabricas.index', 'uses' => 'OsFabricaController@index'));
                Route::get('/novo', array('as' => 'os.fabricas.get.novo', 'uses' => 'OsFabricaController@getNova'));
                Route::post('/novo', array('as' => 'os.fabricas.post.novo', 'uses' => 'OsFabricaController@postNova'));
                Route::get('/edit/{id}', array('as' => 'os.fabricas.get.edit', 'uses' => 'OsFabricaController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'os.fabricas.post.edit', 'uses' => 'OsFabricaController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'os.fabricas.remove', 'uses' => 'OsFabricaController@getRemove'));
            });

            Route::group(['prefix' => 'parametros'], function () {
                Route::get('/', array('as' => 'os.parametros.index', 'uses' => 'OsParametroController@index'));
                Route::get('/novo', array('as' => 'os.parametros.get.novo', 'uses' => 'OsParametroController@getNova'));
                Route::post('/novo', array('as' => 'os.parametros.post.novo', 'uses' => 'OsParametroController@postNova'));
                Route::get('/edit/{id}', array('as' => 'os.parametros.get.edit', 'uses' => 'OsParametroController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'os.parametros.post.edit', 'uses' => 'OsParametroController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'os.parametros.remove', 'uses' => 'OsParametroController@getRemove'));
            });

            Route::group(['prefix' => 'saidas'], function () {
                Route::get('/', array('as' => 'os.saidas.index', 'uses' => 'OsSaidaController@index'));
                Route::get('/novo', array('as' => 'os.saidas.get.novo', 'uses' => 'OsSaidaController@getNova'));
                Route::post('/novo', array('as' => 'os.saidas.post.novo', 'uses' => 'OsSaidaController@postNova'));
                Route::get('/edit/{id}', array('as' => 'os.saidas.get.edit', 'uses' => 'OsSaidaController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'os.saidas.post.edit', 'uses' => 'OsSaidaController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'os.saidas.remove', 'uses' => 'OsSaidaController@getRemove'));
            });

            Route::group(['prefix' => 'volumes'], function () {
                Route::get('/', array('as' => 'os.volumes.index', 'uses' => 'OsVolumeController@index'));
                Route::get('/novo', array('as' => 'os.volumes.get.novo', 'uses' => 'OsVolumeController@getNova'));
                Route::post('/novo', array('as' => 'os.volumes.post.novo', 'uses' => 'OsVolumeController@postNova'));
                Route::get('/edit/{id}', array('as' => 'os.volumes.get.edit', 'uses' => 'OsVolumeController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'os.volumes.post.edit', 'uses' => 'OsVolumeController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'os.volumes.remove', 'uses' => 'OsVolumeController@getRemove'));
            });

            Route::group(['prefix' => 'servicos'], function () {
                Route::get('/', array('as' => 'os.servicos.index', 'uses' => 'OsServicoController@index'));
                Route::get('/search/{os}', array('as' => 'os.servicos.search', 'uses' => 'OsServicoController@search'));
                Route::get('/novo', array('as' => 'os.servicos.get.novo', 'uses' => 'OsServicoController@getNova'));
                Route::post('/novo', array('as' => 'os.servicos.post.novo', 'uses' => 'OsServicoController@postNova'));
                Route::get('/novo/{id}', array('as' => 'os.servicos.get.novo.copia', 'uses' => 'OsServicoController@getNovaCopia'));
                Route::post('/novo/{id}', array('as' => 'os.servicos.post.novo', 'uses' => 'OsServicoController@postNova'));
                Route::get('/edit/{id}', array('as' => 'os.servicos.get.edit', 'uses' => 'OsServicoController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'os.servicos.post.edit', 'uses' => 'OsServicoController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'os.servicos.remove', 'uses' => 'OsServicoController@getRemove'));
                Route::get('/send/{id}', array('as' => 'os.servicos.send', 'uses' => 'OsServicoController@sendEmail'));
                Route::get('/print/{id}', array('as' => 'os.servicos.imprimir', 'uses' => 'OsServicoController@getImprimir'));
                Route::get('/destinatarios', array('as' => 'os.servicos.get.destinatarios', 'uses' => 'OsServicoController@getDestinatarios'));
                Route::post('/destinatarios', array('as' => 'os.servicos.post.destinatarios', 'uses' => 'OsServicoController@postDestinatarios'));
            });
        });

        Route::group(['middleware' => 'admin'], function () {
            Route::group(['prefix' => 'avaliacoes'], function () {
                Route::get('/nova', array('as' => 'avaliacoes.create', 'uses' => 'AvaliacaoController@getNova'));
                Route::post('/nova', array('as' => 'avaliacoes.create', 'uses' => 'AvaliacaoController@postNova'));
                Route::get('/edit/{id}', array('as' => 'avaliacoes.edit', 'uses' => 'AvaliacaoController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'avaliacoes.edit', 'uses' => 'AvaliacaoController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'avaliacoes.remove', 'uses' => 'AvaliacaoController@getRemove'));
            });

            Route::group(['prefix' => 'colaboradores'], function () {
                Route::get('/', array('as' => 'colaboradores.index', 'uses' => 'ColaboradorController@index'));
                Route::get('/nova', array('as' => 'colaboradores.create', 'uses' => 'ColaboradorController@getNova'));
                Route::post('/nova', array('as' => 'colaboradores.create', 'uses' => 'ColaboradorController@postNova'));
                Route::get('/edit/{id}', array('as' => 'colaboradores.edit', 'uses' => 'ColaboradorController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'colaboradores.edit', 'uses' => 'ColaboradorController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'colaboradores.remove', 'uses' => 'ColaboradorController@getRemove'));
            });

            Route::group(['prefix' => 'competencias'], function () {
                Route::get('/', array('as' => 'competencias.index', 'uses' => 'CompetenciaController@index'));
                Route::get('/nova', array('as' => 'competencias.create', 'uses' => 'CompetenciaController@getNova'));
                Route::post('/nova', array('as' => 'competencias.create', 'uses' => 'CompetenciaController@postNova'));
                Route::get('/edit/{id}', array('as' => 'competencias.edit', 'uses' => 'CompetenciaController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'competencias.edit', 'uses' => 'CompetenciaController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'competencias.remove', 'uses' => 'CompetenciaController@getRemove'));
            });

            Route::group(['prefix' => 'itens-competencias'], function () {
                Route::get('/', array('as' => 'itens.competencias.index', 'uses' => 'ItensCompetenciaController@index'));
                Route::get('/nova', array('as' => 'itens.competencias.create', 'uses' => 'ItensCompetenciaController@getNova'));
                Route::post('/nova', array('as' => 'itens.competencias.create', 'uses' => 'ItensCompetenciaController@postNova'));
                Route::get('/edit/{id}', array('as' => 'itens.competencias.edit', 'uses' => 'ItensCompetenciaController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'itens.competencias.edit', 'uses' => 'ItensCompetenciaController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'itens.competencias.remove', 'uses' => 'ItensCompetenciaController@getRemove'));
            });

            Route::group(['prefix' => 'perfis'], function () {
                Route::get('/', array('as' => 'perfis.index', 'uses' => 'PerfilController@index'));
                Route::get('/nova', array('as' => 'perfis.create', 'uses' => 'PerfilController@getNova'));
                Route::post('/nova', array('as' => 'perfis.create', 'uses' => 'PerfilController@postNova'));
                Route::get('/edit/{id}', array('as' => 'perfis.edit', 'uses' => 'PerfilController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'perfis.edit', 'uses' => 'PerfilController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'perfis.remove', 'uses' => 'PerfilController@getRemove'));
            });

            Route::group(['prefix' => 'usuarios'], function () {
                Route::get('/', array('as' => 'usuarios.index', 'uses' => 'UsuarioController@index'));
                Route::get('/nova', array('as' => 'usuarios.create', 'uses' => 'UsuarioController@getNova'));
                Route::post('/nova', array('as' => 'usuarios.create', 'uses' => 'UsuarioController@postNova'));
                Route::get('/edit/{id}', array('as' => 'usuarios.edit', 'uses' => 'UsuarioController@getEdit'));
                Route::post('/edit/{id}', array('as' => 'usuarios.edit', 'uses' => 'UsuarioController@postEdit'));
                Route::get('/remove/{id}', array('as' => 'usuarios.remove', 'uses' => 'UsuarioController@getRemove'));
            });
        });

        Route::get('/servicos/colaboradores/{id}', array('as' => 'servicos.colaboradores', 'uses' => 'ServicosController@getColaboradores'));
        Route::post('/servicos/media-acao', array('as' => 'servicos.media.acao', 'uses' => 'ServicosController@getMediaAcao'));
        Route::get('/servicos/os/cliente/{id}', array('as' => 'servicos.os.cliente', 'uses' => 'ServicosController@getOsClientes'));
        Route::get('/servicos/os/servicos/cliente/{id}', array('as' => 'servicos.os.servico', 'uses' => 'ServicosController@getOsServicoCliente'));
        Route::get('/servicos/os/check-job/{os}', array('as' => 'servicos.os.check', 'uses' => 'ServicosController@checkJob'));
    });
});
