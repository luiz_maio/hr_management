<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ItensCompetencias;
use App\Competencias;

use App\Http\Requests;

class ItensCompetenciaController extends Controller
{
    private $itensCompetencia;

    public function __construct(ItensCompetencias $itensCompetencia)
    {
    	$this->itensCompetencia = $itensCompetencia;
    }

    public function index()
    {
        $itens = ItensCompetencias::all();
        $itensCompetencias = null;
        foreach ($itens as $item) {
            $item->competencias;
            $itensCompetencias[] = $item;
        }
        return view('itens-competencias.index', compact('itensCompetencias'));
    }

    public function getNova()
    {
        $allCompetencias = Competencias::all();
        foreach ($allCompetencias as $competencia) {
            $competencias[$competencia->id] = $competencia->descricao;
        }
        return view('itens-competencias.nova', compact('competencias'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->descricao) || empty($request->id_competencia)){
            return false;
        }
        $itensCompetencias = $this->itensCompetencia;
        $itensCompetencias->create($request->all());
        return redirect()->route("itens.competencias.index");
    }

    public function getEdit($id)
    {
        $editItensCompetencias = $this->itensCompetencia->find($id);
        $allCompetencias = Competencias::all();
        foreach ($allCompetencias as $competencia) {
            $competencias[$competencia->id] = $competencia->descricao;
        }
        return view('itens-competencias.edit', compact('editItensCompetencias', 'competencias'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->descricao) || empty($request->id_competencia)){
            return false;
        }
        $itensCompetencias = $this->itensCompetencia->find($id);
        $itensCompetencias->update($request->all());
        return redirect()->route("itens.competencias.index");
    }

    public function getRemove($id)
    {
        $itensCompetencias = $this->itensCompetencia->find($id);
        $itensCompetencias->delete();
        return redirect()->route("itens.competencias.index");
    }

}
