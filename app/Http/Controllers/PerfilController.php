<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Perfis;

use App\Http\Requests;

class PerfilController extends Controller
{
    private $perfil;

    public function __construct(Perfis $perfil)
    {
    	$this->perfil = $perfil;
    }

    public function index()
    {
    	$profiles = Perfis::all();
    	return view('perfis.index', compact('profiles'));
    }

    public function getNova()
    {
        return view('perfis.nova');
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome) && empty($request->status)){
            return false;
        }

        $perfil = $this->perfil;
        $perfil->create($request->all());

        return redirect()->route("perfis.index");
    }

    public function getEdit($id)
    {
        $editPerfil = $this->perfil->find($id);
        return view('perfis.edit', compact('editPerfil'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome) && empty($request->status)){
            return false;
        }

        $perfil = $this->perfil->find($id);
        $perfil->update($request->all());

        return redirect()->route("perfis.index");
    }

    public function getRemove($id)
    {
        $perfil = $this->perfil->find($id);

        foreach ($perfil->usuarios as $usuario) {
            $usuario->detach($usuario->id);
        }

        $perfil->delete();

        return redirect()->route("perfis.index");
    }
}
