<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colaboradores;
use App\OsClientes;
use App\OsServicos;

use App\Http\Requests;

class ServicosController extends Controller
{
    public function getColaboradores($id)
    {
        $colaborador = Colaboradores::find($id);
        return response(compact('colaborador'));
    }

    public function getMediaAcao(Request $request)
    {
        $cont = array();
        $medias = array();

        for($i = 1; $i < 5; $i++){
            $cont[$i] = 0;
        }

        foreach ($request->notas as $key => $nota) {
            if($nota != 5)
                $cont[$nota]++;
        }

        foreach ($cont as $key => $value) {
            $medias[$key] = (int)round(($value * 100) / array_sum($cont));
        }

        if($medias[1] + $medias[2] >= 75 && $medias[1] > $medias[2]){
            $nota = 'Excelente';
            $acao = 'Avaliação após 6 meses';
        }
        elseif($medias[1] + $medias[2] >= 75 && $medias[1] <= $medias[2]){
            $nota = 'Bom';
            $acao = 'Avaliação após 6 meses';
        }
        elseif($medias[1] + $medias[2] < 75 && $medias[1] + $medias[2] >= 50){
            $nota = 'Bom';
            $acao = 'Avaliação após 6 meses';
        }
        elseif($medias[1] + $medias[2] < 50 && $medias[3] >= $medias[4]){
            $nota = 'Regular';
            $acao = 'Avaliação após 3 meses';
        }
        elseif($medias[1] + $medias[2] < 50 && $medias[3] <= $medias[4]){
            $nota = 'Ruim';
            $acao = 'Avaliação após 2 meses';
        }

        return response(compact('medias', 'nota', 'acao'));
    }

    public function getOsClientes($id)
    {
        $cliente = OsClientes::find($id);
        $servicos = OsServicos::where('id_cliente', $id)->orderBy('job', 'desc')->first();
        $cliente->fabricas;
        $cliente->parametros;

        if(is_null($servicos) || date('y') !== substr($servicos->job, 0, 2))
            $cliente->job = date('y') . '-0001';
        else
            $cliente->job = date('y') . '-' . str_pad(substr($servicos->job, 3, 4)+1, 4, "0", STR_PAD_LEFT);

        return response(compact('cliente'));
    }

    public function getOsServicoCliente($id)
    {
        $servicos = [];
        $allServicos = OsServicos::where('id_cliente', $id)->orderBy('job')->get();
        foreach ($allServicos as $servico) {
            $data = date_create($servico->dt_entrada);
            $servico->dt_entrada = date_format($data, 'd/m/Y');
            $servicos[] = $servico;
        }
        return response(compact('servicos'));
    }

    function arrayToXml($array, &$xml_user_info)
    {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)){
                    $subnode = $xml_user_info->addChild("$key");
                    array_to_xml($value, $subnode);
                }else{
                    $subnode = $xml_user_info->addChild("item$key");
                    array_to_xml($value, $subnode);
                }
            }else {
                $xml_user_info->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }

    function checkJob($os)
    {
        $servicos = OsServicos::where('job', $os)->get();
        return response(compact('servicos'));
    }
}
