<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colaboradores;
use App\Competencias;
use App\Avaliacoes;
use App\Notas;

use App\Http\Requests;

class AvaliacaoController extends Controller
{
    private $avaliacao;

    public function __construct(Avaliacoes $avaliacao)
    {
    	$this->avaliacao = $avaliacao;
    }

    public function index()
    {
        $avals = Avaliacoes::all();
        $avaliacoes = null;
        foreach ($avals as $item) {
            $item->colaboradores;
            $item->avaliadores;
            $dataEdit = date_create($item->data_avaliacao);
    		$item->data = !is_null($item->data_avaliacao) ? date_format($dataEdit, 'd/m/Y') : '';
            $avaliacoes[] = $item;
        }
        return view('avaliacoes.index', compact('avaliacoes'));
    }

    public function getNova()
    {
        $allColaboradores = Colaboradores::all();
        $allAvaliadores = Colaboradores::where('is_aval', 1)->get();
        $colaboradores = [];
        $avaliadores = [];
        foreach ($allColaboradores as $colaborador) {
            $colaboradores[$colaborador->id] = $colaborador->nome . ' ' . $colaborador->sobrenome;
        }
        foreach ($allAvaliadores as $avaliador) {
            $avaliadores[$avaliador->id] = $avaliador->nome . ' ' . $avaliador->sobrenome;
        }
        return view('avaliacoes.nova', compact('colaboradores', 'avaliadores'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->id_colaborador) || empty($request->id_avaliador)){
            return false;
        }
        $avaliacao = $this->avaliacao;
        $avaliacao->create($request->all());
        return redirect()->route("avaliacoes.index");
    }

    public function getEdit($id)
    {
        $editAvaliacao = $this->avaliacao->find($id);
        $editAvaliacao->colaboradores;
        $editAvaliacao->avaliadores;
        $allAvaliadores = Colaboradores::where('is_aval', 1)->get();
        $avaliadores = null;
        foreach ($allAvaliadores as $avaliador) {
            $avaliadores[$avaliador->id] = $avaliador->nome . ' ' . $avaliador->sobrenome;
        }
        return view('avaliacoes.edit', compact('editAvaliacao', 'avaliadores'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->id_avaliador)){
            return false;
        }
        $avaliacao = $this->avaliacao->find($id);
        $avaliacao->update($request->all());
        return redirect()->route("avaliacoes.index");
    }

    public function getPontuar($id)
    {
        $avaliacoes = null;
        $competencias = null;
        $notas = Notas::all();
        $allCompetencias = Competencias::orderBy('ordem')->get();
        $editAvaliacao = $this->avaliacao->find($id);
        $editAvaliacao->colaboradores;
        $editAvaliacao->avaliadores;
        $dataEdit = date_create($editAvaliacao->data_avaliacao);
        $editAvaliacao->data_avaliacao = !is_null($editAvaliacao->data_avaliacao) ? date_format($dataEdit, 'd/m/Y') : '';
        foreach ($allCompetencias as $value) {
            $totalItens = null;
            foreach ($value->itensCompetencias as $item) {
                $item->nota = null;
                foreach ($editAvaliacao->itensCompetencias as $aval) {
                    if($item->id == $aval->pivot->id_item_competencia){
                        $item->nota = $aval->pivot->id_nota;
                    }
                }
                $totalItens[] = $item;
            }
            $value->itensCompetencias = $totalItens;
            $competencias[] = $value;
        }
        return view('avaliacoes.pontuar', compact('editAvaliacao', 'competencias', 'notas'));
    }

    public function postPontuar($id, Request $request)
    {
        if(empty($request->data_avaliacao)){
            return false;
        }

        $data = $request->all();
        $dt = str_replace('/', '-', $data['data_avaliacao']);
        $data_avaliacao = date_create($dt);
        $data['data_avaliacao'] = date_format($data_avaliacao, 'Y-m-d');
        $avaliacao = $this->avaliacao->with('itensCompetencias', 'notas')->find($id);

        for($i = 1; $i < 5; $i++){
            $cont[$i] = 0;
        }

        $avaliacao->itensCompetencias()->detach();

        foreach ($data['notas'] as $key => $nota) {
            $avaliacao->itensCompetencias()->attach($avaliacao->id, array('id_item_competencia' => $key, 'id_nota' => $nota));
        }

        $avaliacao->update($data);
        return redirect()->route("avaliacoes.index");
    }

    public function getRemove($id)
    {
        $avaliacao = $this->avaliacao->find($id);
        $avaliacao->itensCompetencias()->detach();
        $avaliacao->delete();
        return redirect()->route("avaliacoes.index");
    }
}
