<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OsClientes;
use Illuminate\Support\Facades\File;

use App\Http\Requests;

class OsClienteController extends Controller
{
    private $cliente;

    public function __construct(OsClientes $cliente)
    {
    	$this->cliente = $cliente;
    }

    public function index()
    {
    	$clientes = OsClientes::all();
    	return view('os-clientes.index', compact('clientes'));
    }

    public function getNova()
    {
        return view('os-clientes.nova');
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome) && empty($request->sigla)){
            return false;
        }

        $dados = $request->all();
        $image = isset($dados['logo']) ? $dados['logo'] : null;

        if(!is_null($image)){
            $path = '/images/clientes/';
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$path;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['path_logo'] = $path.$imageName;
        }

        $cliente = $this->cliente;
        $cliente->create($dados);

        return redirect()->route("os.clientes.index");
    }

    public function getEdit($id)
    {
        $editCliente = $this->cliente->find($id);
        return view('os-clientes.edit', compact('editCliente'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome) && empty($request->sigla)){
            return false;
        }

        $cliente = $this->cliente->find($id);
        $dados = $request->all();
        $image = isset($dados['logo']) ? $dados['logo'] : null;

        if(!is_null($image)){
            $path = '/images/clientes/';
            $extension = $image->getClientOriginalExtension();
            $hash = md5(uniqid(rand(), true));
            $imageName = $hash.".".$extension;
            $destinationPath = public_path().$path;
            $upload_success = $image->move($destinationPath, $imageName);
            $dados['path_logo'] = $path.$imageName;

            if(File::isFile(public_path().$cliente->path_logo)){
                File::delete(public_path().$cliente->path_logo);
            }
        }

        $cliente->update($dados);

        return redirect()->route("os.clientes.index");
    }

    public function getRemove($id)
    {
        $cliente = $this->cliente->find($id);

        if(File::isFile(public_path().$cliente->path_logo)){
            File::delete(public_path().$cliente->path_logo);
        }

        $cliente->delete();

        return redirect()->route("os.clientes.index");
    }
}
