<?php

namespace App\Http\Controllers;

use Validator;
use App\Usuarios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index()
    {
    	if(Auth::check()){
            switch (Auth::user()->id_perfil) {
                case 1:
                    return redirect('usuarios');
                    break;
                case 2:
                    return redirect('avaliacoes');
                    break;
                case 3:
                    return redirect('avaliacoes');
                    break;
                case 4:
                    return redirect('os/servicos');
                    break;
                case 5:
                    return redirect('os/servicos');
                    break;
                default:
                    return view('/login');
                    break;
            }
	    }
    	return view('/login');
    }

    public function postLogin(Request $request)
    {
    	$rules = array(
			'email' => 'required|email',
			'senha' => 'required|min:6',
		);

		$messages = array(
	    	'email.required' => 'Digite o Email',
	    	'email.email' => 'Formato de Email Incorreto',
	    	'senha.required' => 'Senha Obrigatória',
	    	'senha.min' => 'A Senha deve conter no Minimo 6 Caracteres',
    	);

    	$validator = Validator::make($request->only('email', 'senha'), $rules, $messages);

        if (!$validator->passes()) {
            return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
        }

    	$access = $request->all();
	    $credentials = [
	        'email' => $access['email'],
	        'password' => $access['senha'],
	        'status' => 1
	    ];

	    if(Auth::attempt($credentials)){
            switch (Auth::user()->id_perfil) {
                case 1:
                    return redirect('usuarios');
                    break;
                case 2:
                    return redirect('avaliacoes');
                    break;
                case 3:
                    return redirect('avaliacoes');
                    break;
                case 4:
                    return redirect('os/servicos');
                    break;
                case 5:
                    return redirect('os/servicos');
                    break;
                default:
                    return view('/login');
                    break;
            }
	    }
	    else{
	    	return redirect('/')
                ->withErrors('Usuário não localizado. Verifique os dados de acesso e tente novamente.')
                ->withInput();
	    }
    }
}
