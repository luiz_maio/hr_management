<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OsSaidas;

use App\Http\Requests;

class OsSaidaController extends Controller
{
    private $saida;

    public function __construct(OsSaidas $saida)
    {
    	$this->saida = $saida;
    }

    public function index()
    {
    	$saidas = OsSaidas::all();
    	return view('os-saidas.index', compact('saidas'));
    }

    public function getNova()
    {
        return view('os-saidas.nova');
    }

    public function postNova(Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $saida = $this->saida;
        $saida->create($request->all());

        return redirect()->route("os.saidas.index");
    }

    public function getEdit($id)
    {
        $editSaida = $this->saida->find($id);
        return view('os-saidas.edit', compact('editSaida'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->nome)){
            return false;
        }

        $saida = $this->saida->find($id);
        $saida->update($request->all());

        return redirect()->route("os.saidas.index");
    }

    public function getRemove($id)
    {
        $saida = $this->saida->find($id);
        $saida->delete();

        return redirect()->route("os.saidas.index");
    }
}
