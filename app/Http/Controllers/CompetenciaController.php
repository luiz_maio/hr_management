<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Competencias;

use App\Http\Requests;

class CompetenciaController extends Controller
{
    private $competencia;

    public function __construct(Competencias $competencia)
    {
    	$this->competencia = $competencia;
    }

    public function index()
    {
        $competencias = Competencias::all();
        return view('competencias.index', compact('competencias'));
    }

    public function getNova()
    {
        $competencias = Competencias::all();
        $ordem = range(1, count($competencias)+1);
        return view('competencias.nova', compact('ordem'));
    }

    public function postNova(Request $request)
    {
        if(empty($request->descricao)){
            return false;
        }

        $data = $request->all();
        $data['ordem'] = $data['ordem'] + 1;

        $reordenarCompetencias = Competencias::where('ordem', '>=', $data['ordem'])->get();
        foreach ($reordenarCompetencias as $value) {
            $novaOrdem = $value->ordem + 1;
            $value->update(['ordem' => $novaOrdem]);
        }

        $competencia = $this->competencia;
        $competencia->create($data);

        return redirect()->route("competencias.index");
    }

    public function getEdit($id)
    {
        $competencias = Competencias::all();
        $ordem = range(1, count($competencias));
        $editCompetencia = $this->competencia->find($id);
        return view('competencias.edit', compact('editCompetencia', 'ordem'));
    }

    public function postEdit($id, Request $request)
    {
        if(empty($request->descricao)){
            return false;
        }

        $data = $request->all();
        $data['ordem'] = $data['ordem'] + 1;

        $reordenarCompetencias = Competencias::where('ordem', '>=', $data['ordem'])->get();
        foreach ($reordenarCompetencias as $value) {
            $novaOrdem = $value->ordem + 1;
            $value->update(['ordem' => $novaOrdem]);
        }

        $competencia = $this->competencia->find($id);
        $competencia->update($data);
        return redirect()->route("competencias.index");
    }

    public function getRemove($id)
    {
        $competencia = $this->competencia->find($id);
        $reordenarCompetencias = Competencias::where('ordem', '>', $competencia->ordem)->get();

        foreach ($competencia->itensCompetencias as $itensCompetencia) {
            $tmpItensCompetencia = $itensCompetencia->find($itensCompetencia->id);
            $tmpItensCompetencia->delete();
        }
        $competencia->delete();

        foreach ($reordenarCompetencias as $value) {
            $novaOrdem = $value->ordem - 1;
            $value->update(['ordem' => $novaOrdem]);
        }

        return redirect()->route("competencias.index");
    }
}
