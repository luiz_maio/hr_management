<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Colaboradores;
use App\Competencias;
use App\Avaliacoes;
use App\Notas;

class PrintController extends Controller
{
    public function getPrint($id)
    {
        $avaliacoes = null;
        $competencias = null;
        $notas = Notas::all();
        $allCompetencias = Competencias::orderBy('ordem')->get();
        $editAvaliacao = Avaliacoes::find($id);
        $editAvaliacao->colaboradores;
        $editAvaliacao->avaliadores;
        $dataEdit = date_create($editAvaliacao->data_avaliacao);
        $editAvaliacao->data_avaliacao = !is_null($editAvaliacao->data_avaliacao) ? date_format($dataEdit, 'd/m/Y') : '';
        foreach ($allCompetencias as $value) {
            $totalItens = null;
            foreach ($value->itensCompetencias as $item) {
                $item->nota = null;
                foreach ($editAvaliacao->itensCompetencias as $aval) {
                    if($item->id == $aval->pivot->id_item_competencia){
                        $item->nota = $aval->pivot->id_nota;
                    }
                }
                $totalItens[] = $item;
            }
            $value->itensCompetencias = $totalItens;
            $competencias[] = $value;
        }
        return view('avaliacoes.print', compact('editAvaliacao', 'competencias', 'notas'));
    }
}
