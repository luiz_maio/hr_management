<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsFabricas extends Model
{
    protected $fillable = array(
        'nome',
        'sigla',
        'status'
    );

    public function clientes()
    {
    	return $this->belongsToMany('App\OsClientes', 'os_clientes_fabricas', 'id_fabrica', 'id_cliente');
    }
}
