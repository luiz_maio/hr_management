<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsVolumes extends Model
{
    protected $fillable = array(
        'nome'
    );
}
