<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colaboradores extends Model
{
    protected $fillable = array(
        'nome',
        'sobrenome',
        'cargo',
        'depto',
        'email',
        'is_aval',
		'nome_aval',
        'dest_email_os'
	);

    public function avaliacoes()
    {
    	return $this->hasMany('App\Avaliacoes', 'id_colaborador');
    }

}
