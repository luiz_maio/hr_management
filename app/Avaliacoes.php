<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avaliacoes extends Model
{
    protected $fillable = array(
        'id_colaborador',
        'id_avaliador',
        'nota_acao',
        'obs_acao',
        'obs_finais',
        'data_avaliacao'
	);

    public function colaboradores()
    {
    	return $this->belongsTo('App\Colaboradores', 'id_colaborador');
    }

    public function avaliadores()
    {
    	return $this->belongsTo('App\Colaboradores', 'id_avaliador');
    }

    public function itensCompetencias()
    {
        return $this->belongsToMany('App\ItensCompetencias', 'avaliacoes_itens_competencias_notas', 'id_avaliacao', 'id_item_competencia')
        ->withPivot('id_item_competencia', 'id_nota');
    }

    public function notas()
    {
        return $this->belongsToMany('App\Notas', 'avaliacoes_itens_competencias_notas', 'id_avaliacao', 'id_nota');
    }
}
