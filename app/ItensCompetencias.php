<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItensCompetencias extends Model
{
    protected $fillable = array(
        'id_competencia',
        'descricao'
	);

    public function competencias()
    {
    	return $this->belongsTo('App\Competencias', 'id_competencia');
    }

    public function avaliacoes()
    {
        return $this->belongsToMany('App\Avaliacoes', 'avaliacoes_itens_competencias_notas', 'id_item_competencia', 'id_avaliacao');
    }

    public function notas()
    {
        return $this->belongsToMany('App\Notas', 'avaliacoes_itens_competencias_notas', 'id_item_competencia', 'id_nota');
    }
}
