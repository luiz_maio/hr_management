<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuarios extends Authenticatable
{
    protected $fillable = array(
		'id_perfil',
		'nome',
		'sobrenome',
		'email',
		'senha',
		'cargo',
		'depto',
		'status',
		'remember_token'
	);

    public function getAuthPassword()
    {
    	return $this->senha;
    }

    public function perfis()
    {
    	return $this->belongsTo('App\Perfis', 'id_perfil');
    }
}
