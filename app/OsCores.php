<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OsCores extends Model
{
    protected $fillable = array(
        'id_servico',
        'nome'
    );

    public function servicos()
    {
    	return $this->belongsTo('App\OsServicos', 'id_servico');
    }
}
