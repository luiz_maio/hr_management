<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AvaliacoesCompetenciasNotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes_itens_competencias_notas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_avaliacao')->unsigned();
            $table->foreign('id_avaliacao')->references('id')->on('avaliacoes');
            $table->integer('id_item_competencia')->unsigned();
            $table->foreign('id_item_competencia')->references('id')->on('itens_competencias');
            $table->integer('id_nota')->unsigned();
            $table->foreign('id_nota')->references('id')->on('notas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avaliacoes_itens_competencias_notas');
    }
}
