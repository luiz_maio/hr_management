<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OsServicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('os_servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cliente')->unsigned();
            $table->foreign('id_cliente')->references('id')->on('os_clientes');
            $table->string('rotulo');
            $table->dateTime('dt_entrada')->nullable();
            $table->string('codigo');
            $table->string('codigo_exal');
            $table->string('job');
            $table->string('job_anterior');
            $table->string('tipo_saida');
            $table->string('path_xml');
            $table->string('observacoes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('os_servicos');
    }
}
