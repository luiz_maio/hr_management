<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Avaliacoes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_colaborador')->unsigned();
            $table->foreign('id_colaborador')->references('id')->on('colaboradores');
            $table->integer('id_avaliador')->unsigned();
            $table->foreign('id_avaliador')->references('id')->on('colaboradores');
            $table->string('nota_acao');
            $table->string('obs_acao');
            $table->string('obs_finais');
            $table->dateTime('data_avaliacao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avaliacoes');
    }
}
