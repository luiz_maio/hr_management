<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OsParametros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('os_parametros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tipo_campo')->unsigned();
            $table->foreign('id_tipo_campo')->references('id')->on('os_tipos_campos_xml');
            $table->string('nome');
            $table->string('sigla');
            $table->string('valor')->nullable();
            $table->enum('status', [0,1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('os_parametros');
    }
}
