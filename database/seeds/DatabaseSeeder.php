<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notas = [
            ['descricao' => 'Excelente'],
            ['descricao' => 'Bom'],
            ['descricao' => 'Regular'],
            ['descricao' => 'Ruim'],
            ['descricao' => 'Não Aplicável']
        ];

        DB::table('notas')->insert($notas);

        $tiposCampos = [
            ['nome' => 'Texto'],
            ['nome' => 'Booleano']
        ];

        DB::table('os_tipos_campos_xml')->insert($tiposCampos);

        $perfis = [
            ['nome' => 'Admin', 'status' => 1],
            ['nome' => 'RH', 'status' => 1],
            ['nome' => 'Gestor', 'status' => 1],
            ['nome' => 'OS Admin', 'status' => 1],
            ['nome' => 'OS Usuário', 'status' => 1]
        ];

        DB::table('perfis')->insert($perfis);

        $usuarios = [
            'id_perfil' => 1,
            'nome' => 'Fernando',
            'sobrenome' => 'Maio',
            'email' => 'fernando@paintpack.com.br',
            'senha' => bcrypt('pack2005'),
            'cargo' => 'Programador',
            'depto' => 'Administrativo',
            'status' => 1
        ];

        DB::table('usuarios')->insert($usuarios);
    }
}
