$(function () {
    $('#btn-search').click(function () {
        var src = $('#txt-search').val();
        src.trim() == '' ? window.location = window.location.origin + '/os/servicos' : window.location = window.location.origin + '/os/servicos/search/' + src;
    });

    $("#filtroCliente").change( function() {
        $.ajax({
            type: 'get',
            url: window.location.origin + '/servicos/os/servicos/cliente/' + $(this).val(),
            context: document.body
        }).done(function(data){
            $('#tabela-corpo').empty();
            getServicos(data.servicos);
        });
    });
});

function getServicos(data)
{
    var val = '<tr>';
    if(data.length > 0){
        $(data).each(function( index, value ) {
            val += '    <td>' + value.rotulo + '</td>';
            val += '    <td>' + value.job + '</td>';
            val += '    <td>' + value.codigo + '</td>';
            val += '    <td>' + value.dt_entrada + '</td>';
            val += '    <td>';
            val += '        <a href="servicos/edit/' + value.id + '" id="btn-edit" class="btn btn-primary btn-sm">Editar</a>'
            val += '        <a href="servicos/remove/' + value.id + '" id="btn-remove" class="btn btn-danger btn-sm" onclick="return confirm("Deseja remover esse serviço?");>Remover</a>'
            val += '        <a href="servicos/print/' + value.id + '" id="btn-print" class="btn btn-info btn-sm" target="_blank">Imprimir OS</a>'
            val += '        <a href="servicos/send/' + value.id + '" id="btn-send" class="btn btn-warning btn-sm" onclick="return confirm("Deseja enviar essa OS?");>Enviar</a>'
            val += '        <a href="servicos/novo/' + value.id + '" id="btn-send" class="btn btn-success btn-sm">Duplicar OS</a>'
            val += '    </td>';
            val += '</tr>';
        });
    }
    else{
        val += '<td colspan="5">';
        val += '    <h4>Nenhum Serviço Localizado</h4>';
        val += '</td>';
    }
    val += '</tr>';
    $('#tabela-corpo').append(val);
}
