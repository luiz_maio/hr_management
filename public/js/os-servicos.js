$(function () {
	$("form").validate({
		rules:{
			id_cliente:{
				required: true
			},
			tipo_saida:{
				required: true
			},
			volume:{
				required: true
			},
			rotulo:{
				required: true
			},
			job_num:{
				required: true
			},
			job_rev:{
				required: true
			},
			dt_entrada:{
				required: true
			},
			codigo:{
				required: true
			},
			'fabricas[]':{
				required: true,
				minlength: 1
			}
		},
		messages:{
			id_cliente:{
				required: "Selecione o Cliente "
			},
			tipo_saida:{
				required: "Selecione o Tipo de Saída"
			},
			volume:{
				required: "Selecione o Volume"
			},
			rotulo:{
				required: "Preencha o Rótulo"
			},
			job_num:{
				required: "Preencha o Numero do Job"
			},
			job_rev:{
				required: "Preencha a Revisão"
			},
			dt_entrada:{
				required: "Preencha a Data de Entrada"
			},
			codigo:{
				required: "Preencha o Código do Cliente"
			},
			'fabricas[]':{
				required: "Selecione as Fábricas"
			}
		}
	});

    $("#editEntrada").mask("99/99/2099");
	$("#editJobNum").mask("99-9999");

	$("#editClientes").change( function() {
		$('#editJobNum').val('');
		$('#editJobCli').val('');
        $('#editRevisao').val('');
        $('#lista-fabricas').empty();
        $('#lista-parametros').empty();
		$('#editJobAnterior').removeAttr('disabled');
		$.ajax({
			type: 'get',
			url: window.location.origin + '/servicos/os/cliente/' + $(this).val(),
			context: document.body
		}).done(function(data){
			$('#editJobNum').val(data.cliente.job);
			$('#editJobCli').val(data.cliente.sigla);
			$('#editRevisao').val('1');
			$("#editJobAnterior").mask("99-9999-" + data.cliente.sigla + "-R9");
            if(data.cliente.nome == 'Exal')
				$('#campo-exal').show();
            else
				$('#campo-exal').hide();
            getFabricas(data.cliente.fabricas);
            getParametros(data.cliente.parametros);
		});
	});

	$("#btn-cores").click(function() {
		var val = '<option selected="selected">Selecione uma Cor...</option>';
		val += '<option value="Aluminio">Aluminio</option>';
		$('#box-cores input[type=text]').each(function() {
			if($(this).val() != '')
			val += '<option value="' + $(this).val() + '">' + $(this).val() +'</option>';
		});
		$('.selectCores').empty().append(val);
		$('.selectCores').removeAttr('readonly');
		$('.selectCores').removeAttr('disabled');
	});

	$("#editRevisao").blur(function() {
		if($("#editRevisao").val() != ''){
			var os = $("#editJobNum").val() + "-" + $("#editJobCli").val() + "-" + 'R' + $("#editRevisao").val();
			$.ajax({
				type: 'get',
				url: window.location.origin + '/servicos/os/check-job/' + os,
				context: document.body
			}).done(function(data){
				console.log(data.servicos.length);
				if(data.servicos.length > 0){
					alert('O job informado já está cadastrado no sistema.');
					$("#editJobNum").val('');
					$("#editRevisao").val('');
					$("#editJobNum").focus();
					return false;
				}
			});
		}
	});

});

function getFabricas(fabricas)
{
    var val = '';
    var cont = 1;
    $(fabricas).each(function( index, value ) {
        val += '<div class="col-sm-2 control-label" style="text-align: left">';
        val += '	<input id="editFabrica[' + value.id + ']" name="fabricas[]" type="checkbox" value="' + value.sigla + '">&nbsp;';
        val +=          value.nome;
        val += '</div>';
        if(cont % 5 == 0){
            val += '<br /><br />';
            val += '<label for="lblFabricas" class="col-sm-1 control-label"></label>';
        }
        cont++;
    });

    $('#lista-fabricas').append(val);
}

function getParametros(parametros)
{
    var val = '';
    var cont = 1;
    $(parametros).each(function( index, value ) {
		if(value.sigla.indexOf("COR") >= 0){
			val += '<label for="lblParametros" class="col-sm-2 control-label">' + value.nome + '</label>';
	        val += '<div class="col-sm-4" style="float: left;">';
			val += '	<select class="form-control selectCores" name="parametros[' + value.sigla + ']" disabled>';
			val += '		<option>Selecione uma Cor...</option>';
			val += '	</select>';
			val += '</div>';
		}
		else if(value.id_tipo_campo == 1){
			val += '<label for="lblParametros" class="col-sm-2 control-label">' + value.nome + '</label>';
	        val += '<div class="col-sm-4" style="float: left;">';
			val += '	<input class="form-control" name="parametros[' + value.sigla + ']" type="text">';
			val += '</div>';
		}
		else if(value.id_tipo_campo == 3){
			val += '<input name="parametros[' + value.sigla + ']" type="hidden" value="' + value.valor + '">';
		}
		else{
			val += '<label for="lblParametros" class="col-sm-2 control-label">' + value.nome + '</label>';
	        val += '<div class="col-sm-4" style="float: left;">';
			val += '	<input type="radio" name="parametros[' + value.sigla + ']" value="1"> Sim';
			val += '	&emsp;';
			val += '	<input type="radio" name="parametros[' + value.sigla + ']" value="0"> Não';
			val += '</div>';
		}
        if(cont % 2 == 0){
            val += '<br /><br /><br />';
        }
        cont++;
    });

    $('#lista-parametros').append(val);
}
